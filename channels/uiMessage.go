package channels

const (
	UILoadPlaylist = 0
	UIChangeSong   = 1
	UISetProgress  = 2
)

type UIMessage struct {
	MessageType int
	Message     interface{}
}

var UIMessages chan UIMessage

func init() {
	UIMessages = make(chan UIMessage, 100)
}
