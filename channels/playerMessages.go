package channels

const (
	PlayNextSong     = 0
	PlayPreviousSong = 1
	TogglePause      = 2
)

type PlayerMessage struct {
	Type    int
	Message interface{}
}

var PlayerMessages chan PlayerMessage

func init() {
	PlayerMessages = make(chan PlayerMessage)
}
