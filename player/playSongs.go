package player

import (
	ch "gitlab.com/almaember/miniplay/channels"
)

var currentSong int = 0

func playList(songs []string) {
	ch.UIMessages <- ch.UIMessage{
		MessageType: ch.UILoadPlaylist,
		Message:     songs}

	ch.UIMessages <- ch.UIMessage{
		MessageType: ch.UIChangeSong,
		Message:     currentSong}

	for {
		msg := <-ch.PlayerMessages

		switch msg.Type {
		case ch.PlayNextSong:
			if currentSong >= len(songs)-1 {
				break
			}
			ch.UIMessages <- ch.UIMessage{
				MessageType: ch.UIChangeSong,
				Message:     currentSong + 1}
			currentSong += 1
		case ch.PlayPreviousSong:
			if currentSong <= 0 {
				break
			}
			ch.UIMessages <- ch.UIMessage{
				MessageType: ch.UIChangeSong,
				Message:     currentSong - 1}
			currentSong -= 1
		}
	}
}
