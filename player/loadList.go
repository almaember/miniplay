package player

import (
	"log"
	"os"
	fp "path/filepath"
	"strings"

	"gitlab.com/almaember/miniplay/util"
)

func PlayListFile(filename string) {
	contentBytes, err := os.ReadFile(filename)
	if err != nil {
		log.Fatalf("Error opening list file: %s", err)
	}

	os.Chdir(fp.Dir(filename))

	songs := strings.Split(util.Stripchars(string(contentBytes), "\r"), "\n")

	playList(songs)
}
