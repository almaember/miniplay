package ui

import (
	"fmt"
	"log"

	"github.com/jroimartin/gocui"
)

// layout manager
func layoutManager(g *gocui.Gui) error {
	maxX, maxY := g.Size()

	// the song list should take up roughly 3/4 of the screen horizontally
	// and 7/8 vertically
	if v, err := g.SetView("song", 0, 0, 3*(maxX/4)-1, 7*(maxY/8)-1); err != nil {
		if err != gocui.ErrUnknownView {
			log.Fatalln("Some error happened with `song` view!")
		}

		fmt.Fprintln(v, "The song list will be here, soon...")
	}

	// the primary UI 1/8 of the screen under the song list
	// it is as wide as the song list
	if v, err := g.SetView("bottom-ui", 0, 7*(maxY/8), 3*(maxX/4)-1, maxY-1); err != nil {
		if err != gocui.ErrUnknownView {
			log.Fatalln("Some error happened with the `bottom-ui` view!")
		}

		fmt.Fprintln(v, "This will be the UI!")
	}

	// the infobar fill 1/4 of the screen on the right
	// there is no "if" because this will always have to be updated
	{
		v, err := g.SetView("infobar", 3*(maxX/4), 0, maxX-1, maxY-1)
		if err != gocui.ErrUnknownView && err != nil {
			log.Fatalln("Some error happened with the `infobar` view!")
		}

		v.Clear()
		fmt.Fprintln(v, wordWrap(strs.infobar, maxX-1-3*(maxX/4)))
	}

	return nil
}
