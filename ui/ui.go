package ui

import (
	"log"
	"os"

	"github.com/jroimartin/gocui"
	ch "gitlab.com/almaember/miniplay/channels"
)

func setup() {
	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		log.Panicln(err)
	}
	defer g.Close()

	g.SetManagerFunc(layoutManager)

	// keybinds
	if err := g.SetKeybinding("", 'q', gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}

	if err := g.SetKeybinding("", 'n', gocui.ModNone, nextSong); err != nil {
		log.Panicln(err)
	}

	if err := g.SetKeybinding("", 'p', gocui.ModNone, prevSong); err != nil {
		log.Panicln(err)
	}

	go updateLoop(g)

	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}

	os.Exit(0)
}

func quit(g *gocui.Gui, v *gocui.View) error {
	g.Close()
	return gocui.ErrQuit
}

func nextSong(g *gocui.Gui, v *gocui.View) error {
	ch.PlayerMessages <- ch.PlayerMessage{Type: ch.PlayNextSong, Message: nil}
	return nil
}

func prevSong(g *gocui.Gui, v *gocui.View) error {
	ch.PlayerMessages <- ch.PlayerMessage{Type: ch.PlayPreviousSong, Message: nil}
	return nil
}
