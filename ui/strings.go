package ui

var strs struct {
	infobar string
}

func init() {
	strs.infobar = `Keybinds:
Q - Quit
N - Next song
P - Previous song
Right arrow - jump forward 5 seconds
Left arrow - jump backwards 5 seconds
Space - pause/unpause`
}
