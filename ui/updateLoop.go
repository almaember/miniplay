package ui

import (
	"fmt"
	"log"

	"github.com/jroimartin/gocui"

	ch "gitlab.com/almaember/miniplay/channels"
)

var playlist []string

// goroutine to run in the background and update the UI as neccesary
func updateLoop(g *gocui.Gui) {
	for {
		msg := <-ch.UIMessages

		switch msg.MessageType {
		case ch.UILoadPlaylist:
			playlist = msg.Message.([]string)
		case ch.UIChangeSong:
			updateSongList(msg.Message.(int), g)
		}
	}
}

func updateSongList(songNo int, g *gocui.Gui) {
	g.Update(func(g *gocui.Gui) error {
		v, err := g.View("song")

		if err != nil {
			log.Fatalln("Error while updating song list: ", err)
		}
		v.Clear()
		_, termHeight := v.Size()
		textToOutput := getSongListText(songNo, termHeight)
		fmt.Fprintln(v, textToOutput)
		return nil
	})
}

func getSongListText(no, height int) string {
	songNum := int(float32(height) * 0.75)
	var sliceBefore, sliceAfter []string
	var paddedBefore, paddedAfter []string
	paddedBefore = make([]string, 0)
	paddedAfter = make([]string, 0)

	if no >= songNum {
		sliceBefore = playlist[no-songNum : no]
	} else {
		sliceBefore = playlist[:no]
	}
	for _, song := range sliceBefore {
		paddedBefore = append(paddedBefore, song)
	}

	if no < len(playlist)-songNum-1 {
		sliceAfter = playlist[no+1:]
	} else {
		sliceAfter = playlist[no+1:]
	}
	for _, song := range sliceAfter {
		paddedAfter = append(paddedAfter, song)
	}

	paddedCurrent := ">>> " + playlist[no] + "\n"

	var result string

	for _, song := range sliceBefore {
		result += "    " + song + "\n"
	}

	result += paddedCurrent

	for _, song := range sliceAfter {
		result += "    " + song + "\n"
	}

	return result
}
