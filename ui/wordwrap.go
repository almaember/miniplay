package ui

import (
	s "strings"
)

// returns the length of the last line of a string
func getCurrentLength(str string) int {
	lines := s.Split(str, "\n")
	return len(lines[len(lines)-1])
}

func wordWrap(text string, lineWidth int) string {
	var result string
	lines := s.Split(text, "\n")

	for _, line := range lines {
		words := s.Split(line, " ")
		var lineResult string

		for _, word := range words {
			lineTmp := lineResult + word + " "
			currentLenght := getCurrentLength(lineTmp)
			if !(currentLenght > lineWidth) {
				lineResult = lineTmp
			} else {
				lineResult += "\n" + word + " "
			}
		}

		result += lineResult + "\n\n"
	}

	return result
}
