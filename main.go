package main

import (
	"flag"
	"log"

	"gitlab.com/almaember/miniplay/player"
	"gitlab.com/almaember/miniplay/ui"
)

var Options struct {
	listName string // name of the playlist file

	mix  bool
	loop bool
}

func init() {
	// option flags
	flag.BoolVar(&Options.mix, "mix", false, "Set to true to play songs in random order")
	flag.BoolVar(&Options.loop, "loop", false, "Play until manually stopped, instead of when the list ends")
}

func main() {
	log.SetPrefix("Miniplay: ")
	log.SetFlags(0)
	flag.Parse()

	// the first (and only) positional argument is the list name
	Options.listName = flag.Arg(0)

	// you can't play an empty list
	if Options.listName == "" {
		log.Fatalln("No list name provided! Stopping.")
	}

	// start the UI thread
	go ui.StartUI()

	// call the player
	player.PlayListFile(Options.listName)
}
